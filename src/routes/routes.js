import { createRouter, createWebHistory } from "vue-router";
import Detail from "../components/Detail.vue";
import Posts from "../components/Posts.vue";

const routes = [
    {
        path: "/public-app/",
        component: Posts
    },
    {
        path: "/public-app/posts/detail/:id",
        component: Detail
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router;