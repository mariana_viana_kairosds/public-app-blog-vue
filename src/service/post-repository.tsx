import axios from 'axios';


const baseUrl = "https://local-apps.kairosds.com/api";

export const GetPostRepository = async () => {
  const response = await axios.get(`${baseUrl}/posts`, {
    headers: {
      "Content-Type": "application/json",
    }
  })
  return response.data;
}

