FROM node:16-alpine3.14 as builder
RUN mkdir /public-app
WORKDIR /public-app
COPY . .
RUN npm ci
RUN npm run build
RUN npm prune --production

FROM nginx:1.23.1-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /public-app/dist/ /usr/share/nginx/html/public-app
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
